FROM openjdk:8 AS PREPARE

RUN wget https://github.com/metafacture/metafacture-core/releases/download/metafacture-core-5.0.1/metafacture-core-5.0.1-dist.tar.gz
RUN tar xfz  metafacture-core-5.0.1-dist.tar.gz
RUN git clone https://gitlab.com/swissbib/linked/swissbib-metafacture-commands.git
RUN cd swissbib-metafacture-commands && ./gradlew clean shadowJar --refresh-dependencies -x test

FROM openjdk:8-jre-alpine

# This is still necessary even thougth the issue says it is fixed.
RUN apk update
RUN apk upgrade
RUN apk add bash

#see https://github.com/docker-library/openjdk/issues/289
RUN apk add --no-cache nss

COPY --from=PREPARE metafacture-core-5.0.1-dist /app
COPY --from=PREPARE swissbib-metafacture-commands/metafacture-kafka/build/libs  /app/plugins
COPY --from=PREPARE swissbib-metafacture-commands/metafacture-biblio/build/libs  /app/plugins
COPY --from=PREPARE swissbib-metafacture-commands/metafacture-io/build/libs  /app/plugins
COPY --from=PREPARE swissbib-metafacture-commands/metafacture-mangling/build/libs  /app/plugins
COPY --from=PREPARE swissbib-metafacture-commands/metafacture-commons/build/libs  /app/plugins
COPY --from=PREPARE swissbib-metafacture-commands/metafacture-csv/build/libs  /app/plugins
COPY --from=PREPARE swissbib-metafacture-commands/metafacture-elasticsearch/build/libs  /app/plugins
COPY --from=PREPARE swissbib-metafacture-commands/metafacture-json/build/libs  /app/plugins
COPY --from=PREPARE swissbib-metafacture-commands/metafacture-linkeddata/build/libs  /app/plugins
COPY --from=PREPARE swissbib-metafacture-commands/metafacture-swissbib/build/libs  /app/plugins
COPY --from=PREPARE swissbib-metafacture-commands/metafacture-xml/build/libs  /app/plugins
COPY --from=PREPARE swissbib-metafacture-commands/metamorph/build/libs  /app/plugins
COPY configs/java-options.conf /app/config

ENTRYPOINT ["/app/flux.sh"]

CMD ["cbsadapter.flux"]






