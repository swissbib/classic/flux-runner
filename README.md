## Micro Service: Flux Runner

A container which can run any flux file with the available Metafacture commands. 

See Metafacture and Metafacture Swissbib Commands for details.

## Usage 

Mount your flux file as mount and directly run the container as daemon process:
```
docker run -d --rm -v /path/to/flux/file.flux:/flux/file.flux flux-runner /flux/file.flux
```

Example docker-compose service:
```
version: "3"

services:
  sb-all-ingest:
    image: swissbib/flux-runner:1.0.0
    command: /flux/cbsadapter.flux
    volumes:
      - /path/to/flux/file:/flux/cbsadtapter.flux

```